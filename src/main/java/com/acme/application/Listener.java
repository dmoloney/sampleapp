package com.acme.application;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import redis.clients.jedis.Jedis;

import java.io.IOException;


public class Listener implements Runnable{

    private final static String QUEUE_NAME = "tickets";

    public void run() {

        try {
            listen();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void listen() throws IOException, InterruptedException {
        Jedis jedis = new Jedis("192.168.50.10");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.50.10");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(QUEUE_NAME, true, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());
            System.out.println(" [x] Received '" + message + "'");
            jedis.set(delivery.getProperties().getMessageId(), message);
            System.out.println(" [x] Written  '" + message + "'");

        }
    }

}

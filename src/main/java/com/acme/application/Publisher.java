package com.acme.application;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;
import java.text.MessageFormat;

public class Publisher implements Job {

    private final static String QUEUE_NAME = "tickets";

    private Channel channel;

    public Publisher() throws IOException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.50.10");
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {


        try {
            publish();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void publish() throws IOException {


        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        String message = "Hello World!";
        AMQP.BasicProperties.Builder m = new AMQP.BasicProperties.Builder();
        m.messageId(MessageFormat.format("message_{0}", System.currentTimeMillis()));
        channel.basicPublish("", QUEUE_NAME, m.build(), message.getBytes());
        System.out.println("Message sent.");
    }


}

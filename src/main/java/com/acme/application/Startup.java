package com.acme.application;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class Startup {

    public static void main(String[] args) throws SchedulerException {

        JobDetail job = JobBuilder.newJob(Publisher.class)
                .build();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                                .withIntervalInSeconds(5).repeatForever())
                .build();

        // schedule it
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();

        scheduler.scheduleJob(job, trigger);


        Thread listener = new Thread(new Listener());
        listener.start();

    }
}
